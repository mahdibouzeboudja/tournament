#!/usr/bin/env bash

# start mongo image docker
docker run -d -p 27017:27017 mongo:7.0

sleep 5

# launch application
./gradlew bootRun &

sleep 5

# populate data base with data to test
curl --request DELETE \
  --url http://localhost:8080/players \
  --header 'Content-Type: application/json'

while IFS=, read  -r name; do
  SCORE=$((RANDOM % 1000))
  curl --request POST \
    --url http://localhost:8080/players \
    --header 'Content-Type: application/json' \
    --data '{
  	  "nickname": "'"${name}"'",
  	  "score": "'"${SCORE}"'"
    }'
done  < "${PROJECT_DIR}/script/nicknames.txt"