package com.betclic.tournament.arch

import com.tngtech.archunit.core.importer.ImportOption
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest

import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses


@AnalyzeClasses(
    packages = ["com.betclic.tournament"],
    importOptions = [ImportOption.DoNotIncludeTests::class]
)
class ArchRulesTest {

    @ArchTest
    val `domain should not include other packages than domain` = noClasses()
            .that()
            .resideInAPackage("..domain..")
            .should()
            .dependOnClassesThat()
            .resideInAnyPackage("..infra..", "..api..", "..bootstrap..")
}