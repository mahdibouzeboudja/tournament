package com.betclic.tournament.domain.players

import com.betclic.tournament.domain.common.SortingOrder
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow

class PlayerRepositoryTestAdapter: PlayersRepository {

    private val players = mutableListOf<Player>()
    override fun findAllSortingBy(sortingProperty: PlayerSortingProperty?, sortOrder: SortingOrder): Flow<Player> {
        when(sortingProperty) {
            PlayerSortingProperty.SCORE -> when(sortOrder) {
                SortingOrder.ASC -> players.sortBy { it.score }
                SortingOrder.DESC -> players.sortByDescending { it.score }
            }
            PlayerSortingProperty.NICKNAME -> when(sortOrder) {
                SortingOrder.ASC -> players.sortBy { it.nickname }
                SortingOrder.DESC -> players.sortByDescending { it.nickname }
            }
            else -> players.sortBy { it.score }
        }
        return players.asFlow()
    }

    override suspend fun save(player: Player): Player {
        players.removeIf { it.id == player.id }
        players.add(player)
        return player
    }

    override suspend fun findByNickname(nickname: String): Player? {
        return players.find { it.nickname == nickname }
    }

    override suspend fun findByPlayerId(playerId: String): Player? {
        return players.find { it.id == playerId }
    }

    override suspend fun deleteAll() = cleanup()


    fun cleanup() = players.clear()

    fun addAllPlayers(players: List<Player>) = this.players.addAll(players)

}