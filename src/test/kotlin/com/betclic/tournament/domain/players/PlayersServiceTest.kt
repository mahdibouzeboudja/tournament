package com.betclic.tournament.domain.players

import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class PlayersServiceTest {

    private val playerRepository =  PlayerRepositoryTestAdapter()

    private val playerService = PlayersService(playerRepository)

    @Test
    fun `should add player and return its id when no player exists with same nickname`() = runBlocking {
        //given
        val playerNickname = "player_nickname"
        //when
        val result = playerService.createPlayer(playerNickname, 0)
        //then
        assertTrue(result is PlayerCreationSuccessResult)
        assertNotNull(playerRepository.findByNickname(playerNickname))
        //cleanup
        playerRepository.cleanup()
    }

    @Test
    fun `should not add player when there existing player with same nickname`() = runBlocking {
        //given
        val playerNickname = "player_nickname"
        playerRepository.save(Player("id", playerNickname, 0))
        //when
        val result = playerService.createPlayer(playerNickname, 0)
        //then
        assertTrue(result is PlayerCreationErrorResult)
        //cleanup
        playerRepository.cleanup()
    }

    @Test
    fun `should find player by its id`() = runBlocking  {
        //given
        val player = Player("id", "player_nickname", 0)
        playerRepository.save(player)
        //when
        val result = playerService.getPlayerById("id")
        //then
        assertEquals(player, result)
        //cleanup
        playerRepository.cleanup()
    }

    @Test
    fun `should update player score`() = runBlocking {
        //given
        val player = Player("id", "player_nickname", 0)
        playerRepository.save(player)
        //when
        playerService.updatePlayerScore(player.id, 10)
        //then
        assertEquals(10, playerRepository.findByPlayerId(player.id)?.score)
        //cleanup
        playerRepository.cleanup()
    }

    @Test
    fun `should return players ordered by their score descending`() = runBlocking {
        //given
        playerRepository.addAllPlayers(listOf(
            Player("id1", "player_nickname_1", 1),
            Player("id2", "player_nickname_2", 2),
            Player("id3", "player_nickname_3", 3),
            Player("id4", "player_nickname_4", 4),
        ))
        val expectedPlayerOrder = listOf(
            Player("id4", "player_nickname_4", 4),
            Player("id3", "player_nickname_3", 3),
            Player("id2", "player_nickname_2", 2),
            Player("id1", "player_nickname_1", 1),
        )
        //when
        val players = playerService.findAllSortedByScore().map { it.first }
        //then
        assertTrue(players.toList() == expectedPlayerOrder)
        //cleanup
        playerRepository.cleanup()
    }

    @Test
    fun `should find player rank`() = runBlocking {
        //given
        playerRepository.addAllPlayers(listOf(
            Player("id1", "player_nickname_1", 1),
            Player("id2", "player_nickname_2", 2),
            Player("id3", "player_nickname_3", 3),
            Player("id4", "player_nickname_4", 4),
        ))
        //then
        assertEquals(4, playerService.getPlayerRanking("id1"))
        assertEquals(3, playerService.getPlayerRanking("id2"))
        assertEquals(2, playerService.getPlayerRanking("id3"))
        assertEquals(1, playerService.getPlayerRanking("id4"))
        assertEquals(null, playerService.getPlayerRanking("fake_id"))
        //cleanup
        playerRepository.cleanup()
    }

    @Test
    fun `should delete all players`() = runBlocking {
        //given
        playerRepository.addAllPlayers(listOf(
            Player("id1", "player_nickname_1", 1),
            Player("id2", "player_nickname_2", 2),
            Player("id3", "player_nickname_3", 3),
            Player("id4", "player_nickname_4", 4),
        ))
        //
        playerService.deleteAllPlayers()
        //then
        assertEquals(0,  playerService.findAllSortedByScore().toList().size)
        //cleanup
        playerRepository.cleanup()
    }

    @Test
    fun getPlayerById() {
    }
}