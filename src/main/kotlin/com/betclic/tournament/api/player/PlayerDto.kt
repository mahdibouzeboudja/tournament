package com.betclic.tournament.api.player

import com.betclic.tournament.domain.players.Player

data class PlayerDto(val nickname: String, val id: String?, val score: Int?, val ranking: Int?) {
    object PlayerDtoMapper {
        fun Player.toDto(ranking: Int?) = PlayerDto(this.nickname, this.id, score, ranking)
    }
}
