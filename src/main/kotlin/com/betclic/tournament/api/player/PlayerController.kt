package com.betclic.tournament.api.player;

import com.betclic.tournament.api.player.PlayerDto.PlayerDtoMapper.toDto
import com.betclic.tournament.domain.players.PlayerCreationErrorResult
import com.betclic.tournament.domain.players.PlayerCreationResult
import com.betclic.tournament.domain.players.PlayerCreationSuccessResult
import com.betclic.tournament.domain.players.PlayersService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.net.URI

@RestController
@RequestMapping("/players")
class PlayerController(private val playersService: PlayersService) {

    private val buildPlayerLocation = { playerId: String -> URI.create("/players/$playerId") }

    @GetMapping
    fun getPlayers(): Flow<PlayerDto> {
        return playersService
            .findAllSortedByScore()
            .map { it.first.toDto(it.second) }
    }

    @PostMapping
    suspend fun createPlayer(@RequestBody playerDto: PlayerDto): ResponseEntity<PlayerCreationResult> {
        return when(val creationResult = playersService.createPlayer(playerDto.nickname, playerDto.score)) {
            is PlayerCreationSuccessResult -> ResponseEntity.created(buildPlayerLocation(creationResult.userId)).build()
            is PlayerCreationErrorResult -> ResponseEntity.ok().body(creationResult)
        }
    }

    @GetMapping("/{playerId}")
    suspend fun getPlayer(@PathVariable playerId: String): ResponseEntity<PlayerDto> {
        return playersService.getPlayerById(playerId)
            ?.let { ResponseEntity.ok(it.toDto(playersService.getPlayerRanking(it.id))) }
            ?: ResponseEntity.notFound().build()
    }

    @PatchMapping("/{playerId}")
    suspend fun updatePlayerScore(@PathVariable playerId: String, @RequestParam newScore: Int): ResponseEntity<Unit> {
        return playersService.updatePlayerScore(playerId, newScore)
            ?.let { ResponseEntity.ok().build() }
            ?: ResponseEntity.notFound().build()
    }

    @DeleteMapping
    suspend fun deleteAllPlayers(): ResponseEntity<Unit> {
        return playersService.deleteAllPlayers()
            .let { ResponseEntity.ok().build() }
    }

}
