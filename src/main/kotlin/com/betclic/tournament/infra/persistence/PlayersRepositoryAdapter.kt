package com.betclic.tournament.infra.persistence;

import com.betclic.tournament.domain.common.SortingOrder
import com.betclic.tournament.domain.players.Player
import com.betclic.tournament.domain.players.PlayerSortingProperty
import com.betclic.tournament.domain.players.PlayerSortingProperty.*
import com.betclic.tournament.domain.players.PlayersRepository
import com.betclic.tournament.infra.persistence.PlayerDocument.ModelMapper.toPlayer
import com.betclic.tournament.infra.persistence.PlayerDocument.ModelMapper.toPlayerDocument
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Component

@Component
class PlayersRepositoryAdapter(private val repository: PlayerCoroutineCrudRepository): PlayersRepository {

    override fun findAllSortingBy(sortingProperty: PlayerSortingProperty?, sortOrder: SortingOrder): Flow<Player> {
        val sort =  when(sortingProperty) {
            SCORE -> Sort.by(Sort.Direction.DESC, "score")
            NICKNAME -> Sort.by(Sort.Direction.ASC, "nickName")
            else ->  Sort.by(Sort.Direction.ASC, "nickName")
        }
        val sortWithOrder = when(sortOrder) {
            SortingOrder.ASC -> sort.ascending()
            SortingOrder.DESC -> sort.descending()
        }
        return repository.findAll(sortWithOrder).map { it.toPlayer() }
    }

    override suspend fun save(player: Player) = repository.save(player.toPlayerDocument()).toPlayer()

    override suspend fun findByNickname(nickname: String) = repository.findOneByNickName(nickname)?.toPlayer()

    override suspend fun findByPlayerId(playerId: String): Player? = repository.findById(playerId)?.toPlayer()

    override suspend fun deleteAll() = repository.deleteAll()
}
