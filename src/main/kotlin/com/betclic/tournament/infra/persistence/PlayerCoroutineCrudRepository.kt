package com.betclic.tournament.infra.persistence

import org.springframework.data.repository.kotlin.CoroutineCrudRepository
import org.springframework.data.repository.kotlin.CoroutineSortingRepository


interface PlayerCoroutineCrudRepository:
    CoroutineCrudRepository<PlayerDocument, String>, CoroutineSortingRepository<PlayerDocument, String>
{
    suspend fun findOneByNickName(nickname: String): PlayerDocument?
}