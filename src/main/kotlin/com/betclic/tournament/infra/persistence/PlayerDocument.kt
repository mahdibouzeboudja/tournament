package com.betclic.tournament.infra.persistence

import com.betclic.tournament.domain.players.Player
import org.springframework.data.mongodb.core.mapping.Document

@Document("players")
data class PlayerDocument(val id: String, val nickName: String, val score: Int) {
    object ModelMapper{
        fun Player.toPlayerDocument(): PlayerDocument = PlayerDocument(this.id, this.nickname, this.score)
        fun PlayerDocument.toPlayer(): Player = Player(this.id, this.nickName, this.score)

    }
}
