package com.betclic.tournament.domain.players

import com.betclic.tournament.domain.common.SortingOrder
import com.betclic.tournament.domain.players.PlayerSortingProperty.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.withIndex
import java.util.UUID.*

class PlayersService(private val playersRepository: PlayersRepository) {


    fun findAllSortedByScore(): Flow<Pair<Player, Int>> {
        return playersRepository
            .findAllSortingBy(SCORE, SortingOrder.DESC)
            .withIndex()
            .map { Pair(it.value, it.index + 1) }
    }

    suspend fun getPlayerRanking(playerId: String): Int? {
        return findAllSortedByScore()
                .firstOrNull { it.first.id == playerId }
                ?.second
    }

    suspend fun createPlayer(nickname: String, score: Int?): PlayerCreationResult {
        return playersRepository.findByNickname(nickname)
            ?.let { PlayerCreationErrorResult(nickname, "$nickname nickname already exists") }
            ?: PlayerCreationSuccessResult(
                playersRepository.save(Player(randomUUID().toString(), nickname, score ?: 0)).id
            )
    }

    suspend fun getPlayerById(playerId: String): Player? = playersRepository.findByPlayerId(playerId)

    suspend fun updatePlayerScore(playerId: String, score: Int): Int? {
        return playersRepository.findByPlayerId(playerId)
            ?.let { playersRepository.save(it.copy(score = score)).score }
    }

    suspend fun deleteAllPlayers() = playersRepository.deleteAll()


}