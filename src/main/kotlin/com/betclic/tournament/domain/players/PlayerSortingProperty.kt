package com.betclic.tournament.domain.players

enum class PlayerSortingProperty(val description: String) {
    SCORE("Sorting players by their scores"),
    NICKNAME("Sorting players by their names");
}