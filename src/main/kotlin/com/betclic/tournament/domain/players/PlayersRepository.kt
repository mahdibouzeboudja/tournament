package com.betclic.tournament.domain.players

import com.betclic.tournament.domain.common.SortingOrder
import kotlinx.coroutines.flow.Flow


interface PlayersRepository {

    fun findAllSortingBy(sortingProperty: PlayerSortingProperty?, sortOrder: SortingOrder): Flow<Player>

    suspend fun save(player: Player): Player

    suspend fun findByNickname(nickname: String): Player?

    suspend fun findByPlayerId(playerId: String): Player?

    suspend fun deleteAll()
}