package com.betclic.tournament.domain.players

data class Player(val id: String, val nickname: String, val score: Int)
