package com.betclic.tournament.domain.players

sealed interface PlayerCreationResult

class PlayerCreationErrorResult(val nickname: String, val error: String): PlayerCreationResult

class PlayerCreationSuccessResult(val userId: String): PlayerCreationResult