package com.betclic.tournament.domain.common

enum class SortingOrder(private val description: String) {
    DESC("descending order"),
    ASC("ascending order");
}