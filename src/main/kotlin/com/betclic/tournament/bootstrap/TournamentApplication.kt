package com.betclic.tournament.bootstrap

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories

@SpringBootApplication(scanBasePackages = ["com.betclic.tournament"])
@EnableReactiveMongoRepositories(basePackages = ["com.betclic.tournament"])
class TournamentApplication

fun main(args: Array<String>) {
    runApplication<TournamentApplication>(*args)
}
