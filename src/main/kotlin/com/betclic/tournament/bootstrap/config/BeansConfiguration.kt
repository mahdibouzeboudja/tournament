package com.betclic.tournament.bootstrap.config

import com.betclic.tournament.domain.players.PlayersRepository
import com.betclic.tournament.domain.players.PlayersService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class BeansConfiguration {
    @Bean
    fun playerService(playerRepository: PlayersRepository): PlayersService = PlayersService(playerRepository)

}