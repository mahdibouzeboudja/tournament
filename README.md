# Pour lancer l'application en local
- Il faut avoir la version java 21 installée ([sdkman is great tools for that](https://sdkman.io/) )
- Docker installé ([docker](https://docs.docker.com/))
- Lancer une base de données mongo (avec docker ou autre)
- pour lancer l'appication avec un jeu de données de test il faut lancer le script 
```shell
PROJECT_DIR=$(pwd) script/launch.sh
```

# Questions
- L'ordre des joueurs  acs/desc ?
- Un seul tournoi ? (ou il faut en gérer plusieurs)
- Y a-t-il un nombre maximum de joueurs par tournoi ?
- un score est-il toujours positif ?
- deux joueurs à score égal auront-ils le même classement ?

# Reste à faire 
- Ajouter les tests d'intégration 
- Compléter des tests d'architecture
- Ajouter des tests d'analyse static du code
- Configurer les logs (logback...)
- Ajouter une doc swagger pour l'API
- Conteneuriser l'application (docker ou autre) et pousser l'image vers un registry
- Configurer les metrics (healthCheck, liveness, readiness) via actuator par exemple
- Ajout une étape dans la ci pour analyser l'image docker produite (CVE)
- Ajouter l'infra as code pour déployer l'application vers un ochestrateur de conteneurs (kub, ecs)
- Tester en charge l'application via des outils comme Gatling
